import discord, os

from discord.ui import Button, View
from discord.ext import commands
from discord.ext.commands import bot
from discord.commands import Option

from configparser import ConfigParser

config_object = ConfigParser()

if os.path.exists(os.getcwd() + "/config.ini"):
    config_object.read(os.getcwd() + "/config.ini")

guildID = int(config_object["guildid"])


class Shop(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(guild_ids=[guildID], description="Shows the shop")
    async def shop(self, ctx):
        shop_embed = discord.Embed(
            title="Shop", 
            description="future planned configurable description for shop", 
            color=0x00ff00
            )
        await ctx.respond("This is a temporary location for the shop embed for testing, this message will have a button to view the rest of the shops.", embed=shop_embed)

    " Creates a shop with `/shop create` "
""" yaml code needs to be coverted to ini
    @commands.slash_command(guild_ids=[guildID], description="Creates a shop")
    async def shop_create(
        self,
        ctx: discord.ApplicationContext,
        name: Option(
            str, 
            "Shop Name", 
            max_length=32, 
            min_length=1
        )):
        try:
            with open('config.yaml', 'r') as config_file:
                config = yaml.load(config_file, Loader=yaml.BaseLoader)
        except:
            await ctx.respond(f'Error: Shop channel not found./nPlease create a channel with `/shop_channel`')
        else:
            embed = discord.Embed(
                title = f'Shop: {name}',
                description = 'future planned configurable description for users shops',
                colour = 0x2C2F33 
            )   
            embed.set_author(name=ctx.user.nick)    
            embed.set_footer(text=f'Created by {ctx.author.id}')    
            embed.add_field(name='Place holder item name', value='Place holder item price')

            channelID = int(config["Shop Channel"])
            channel = self.bot.get_channel(channelID)
            try:
                await channel.send(embed=embed)
            except:
                await ctx.respond('Error: embed not sent.')
            else:
                await ctx.respond("message sent")
"""
""" More yaml code needs to be coverted to ini
    @commands.slash_command(guild_ids=[guildID], description="Sets shop channel")
    async def shop_channel(
        self,
        ctx: discord.ApplicationContext, 
        channel: discord.TextChannel
    ):
        " sends channel to config file "
        try:
            with open(os.getcwd() + "/config.yaml", "r") as yamlfile:
                cur_yaml = yaml.safe_load(yamlfile)
                cur_yaml.update({"Shop Channel": channel.id})
            if cur_yaml:
                with open(os.getcwd() + "/config.yaml", "w") as yamlfile:
                    yaml.safe_dump(cur_yaml, yamlfile)
        except:
            await ctx.respond('Error: channel not set')
        else:
            await ctx.respond(f"Shop channel set to {channel.mention}")
"""
def setup(bot):
    bot.add_cog(Shop(bot))
