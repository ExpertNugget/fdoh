import discord, os

from discord.ext import commands
from discord.ext.commands import bot

from configparser import ConfigParser

""" old json method
if os.path.exists(os.getcwd() + "/config.json"):
    with open("./config.json")as f:
        configData = json.load(f)

else:
    configTemplate = {
    "Token": "<Bot Token Here>",
    "Prefix": "!",
    "GuildID": "<GuildID Here>"
    }
    with open(os.getcwd() + "/config.json", "w+") as f:
        json.dump(configTemplate, f)
"""
""" old yaml method
if os.path.exists(os.getcwd() + "/config.yaml"):
    with open('config.yaml', 'r') as config_file:
        config = yaml.load(config_file, Loader=yaml.BaseLoader)
else:
    configTemplate = {
    "Token": "<Bot Token Here>",
    "Prefix": "!",
    "GuildID": "<GuildID Here>"
    }
    with open(os.getcwd() + "/config.yaml") as f:
        yaml.dump(configTemplate, f)
"""
config_object = ConfigParser()

if os.path.exists(os.getcwd() + "/config.ini"):
    config_object.read(os.getcwd() + "/config.ini")
else:
    config_object["Discord"] = {
    "Token": "<Bot Token Here>",
    "Prefix": "!; Not used but the bot wont start if left blank",
    "GuildID": "<GuildID Here>"
    }
    with open(os.getcwd() + "/config.ini", "w+") as f:
        config_object.write(f)
    print("Config file created, please fill in the details and restart the bot.")
    quit()


token = config_object["token"]
prefix = config_object["prefix"]
guildID = int(config_object["guildid"])

bot = commands.Bot(command_prefix=[prefix])

for filename in os.listdir("./cogs"):
    if filename.endswith(".py"):
        bot.load_extension(f"cogs.{filename[:-3]}")

@bot.event
async def on_ready():
    print("Bot is ready!")

bot.run(token)
